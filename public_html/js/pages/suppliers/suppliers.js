$(function () {
    //Exportable table
    $('#main1').DataTable({
        dom: 'Bfrtip',
        responsive: true,
        buttons: [
            'excel', 'pdf', 'print'
        ],
        columns: [
            { data: "#" },
            { data: "name" },
            { data: "email" },
            { data: "cNumber" },
			{ data: "address" },
            { data: "type" },
            { data: "remark" }

        ],
        data: supplierList
    });



});



angular.module('suppliers', [])
  .controller('SupplierController', function($scope,$http) {
      $scope.abc="123-34";
      $scope.model={supplierList:[],selected:{}};
      $scope.id=1;

      $scope.sortType = '';
      $scope.sortReverse = false;
      $scope.searchName = '';

      $scope.errorList = [];//this is the array where we push errors.
      
      $scope.addNewSupplier=function (){
          $scope.errorList.length = 0;
          var hasErrors = false;
          if ($scope.name == null){
              $scope.errorList.push({'name':'Please enter supplier name'});
              hasErrors = true;
          }

          if ($scope.type == null){
              $scope.errorList.push({'name':'Please enter supplier type'});
              hasErrors = true;
          }

          if ($scope.email == null){
              $scope.errorList.push({'name':'Please enter supplier email'});
              hasErrors = true;
          }

          if ($scope.address == null){
              $scope.errorList.push({'name':'Please enter supplier address'});
              hasErrors = true;
          }

          if ($scope.cNumber == null){
              $scope.errorList.push({'name':'Please enter supplier contact number'});
              hasErrors = true;
          }

          if ($scope.remark == null){
              $scope.errorList.push({'name':'Please enter supplier remark'});
              hasErrors = true;
          }


          if (hasErrors == true)
          {
              alert("has errors");
              return;
          }

          console.log(hasErrors);
          //$scope.model.supplierList.push({id:$scope.id++,name:$scope.supplier.name,email:$scope.supplier.email,cNumber:$scope.supplier.cNumber, address:$scope.supplier.address,type:$scope.supplier.type,remark:$scope.supplier.remark});
          /*$scope.supplier.name = null;
          $scope.supplier.type = null;
          $scope.supplier.email = null;
		  $scope.supplier.address = null;
          $scope.supplier.cNumber = null;
          $scope.supplier.remark = null;*/
          alert("done");
		  
	  };
      
      $scope.updateSupplier=function (supplier){
          $scope.model.selected = angular.copy(supplier);
          $scope.editable=true;
      };
      
      $scope.deleteSupplier=function (index){
          //$scope.model.selected = angular.copy(supplier);
          //$scope.model.supplierList.splice($scope.model.selected.id,1);
		  $scope.model.supplierList[index] = angular.copy($scope.model.selected);
		  $scope.model.supplierList.splice($scope.model.selected.id,1);
      };
      
      $scope.saveChanges=function (index){
          $scope.model.supplierList[index] = angular.copy($scope.model.selected);
          $scope.resetChanges();
      };
      $scope.resetChanges=function (){
          $scope.model.selected = {};
          $scope.editable=false;
      };

      $scope.init = function(){
          $scope.name = "sajad";
          $scope.type = "sajad";
          $scope.email = "sajad";
          $scope.address = "sajad";
          $scope.cNumber = "sajad";
          $scope.remark = "sajad";
      }
	  

	   
     $scope.clearFields = function(){
         $scope.errorList.length = 0;
          $scope.supplier.name = null;
          $scope.supplier.type = null;
          $scope.supplier.email = null;
		  $scope.supplier.address = null;
          $scope.supplier.cNumber = null;
          $scope.supplier.remark = null;
     };
      
       // gets the template to ng-include for a table row / item
    $scope.getTemplate = function (supplier) {
        if (supplier.id === $scope.model.selected.id) return 'edit';
        else return 'display';
    };

    


});