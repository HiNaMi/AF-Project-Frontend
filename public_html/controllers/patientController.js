'use strict';

angular.module('PharmacyApp').controller('PatientController', ['$scope', '$location', 'PatientService',
    function ($scope, $location, PatientService) {

      // Method to add a patient
      $scope.addPatient = (patient) => {
        PatientService.add(patient).then(() => {
          $('#addPatientModal').modal('hide');
          getPatients();
        });
      }


      // Method to get all the patients
      function getPatients() {
        PatientService.get().then(patients => {
          $scope.patients = patients;
        })
      }

      getPatients();

      $scope.getinfo = () => {
        console.log($scope.patients);
      }
  }]);
