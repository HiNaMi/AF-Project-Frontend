'use strict';

const express = require('express');
const app = express();

app.use('/app',express.static(__dirname + '/public_html'));
// app.use('/modules',express.static(__dirname + "/node_modules"));

app.get('/', function (req, res) {
    res.sendFile(__dirname+'/public_html/index.html')
});

app.listen(5000, err => {
    if (err) {
        console.error(err);
        return;
    }
    console.log('app listening on port 5000');
});