$(function () {
    $('.js-basic-example').DataTable({
        responsive: true
    });

    //Exportable table
    $('.js-exportable').DataTable({
        dom: 'Bfrtip',
        responsive: true,
        buttons: [
            'excel', 'pdf', 'print'
        ],
        //    columns[
        //    {data:"#"}
        //  ]
    });
});


angular.module('patients', [])
    .controller('PatientsController', function($scope,$http) {
        $scope.abc="123-34";
        $scope.model={patientsList:[],selected:{}};
        //$scope.id=0;

        $scope.addNewPatient=function (){
            $scope.model.patientsList.push({id:$scope.id++,title:$scope.patient.title,firstname:$scope.patient.firstname,lastname:$scope.patient.lastname,dateofbirth:$scope.patient.dateofbirth,gender:'',NIC:$scope.patient.NIC,address:$scope.patient.address,phoneNo:$scope.patient.phoneNo});
            //console.log(JSON.stringify($scope.model.drugList));
        };

        $scope.updatePatient=function (patient){
            $scope.model.selected = angular.copy(patient);
            $scope.editable=true;
        };

        $scope.deletePatient=function (){

        };

        $scope.saveChanges=function (index){
            $scope.model.drugList[index] = angular.copy($scope.model.selected);
            $scope.resetChanges();
        };
        $scope.resetChanges=function (){
            $scope.model.selected = {};
            $scope.editable=false;
        };

        // gets the template to ng-include for a table row / item
        $scope.getTemplate = function (patient) {
            if (patient.id === $scope.model.selected.id) return 'edit';
            else return 'display';
        };
    });
