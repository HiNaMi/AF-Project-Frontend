'use strict';
//
// mainapp.config(function($routeProvider) {
//   $routeProvider
//
//     // route for the home page
//     .when('/', {
//       // templateUrl : './pages/dashboard.html',
//       // controller  : 'mainController'
//     })
//
//     // route for the about page
//     .when('/addPatient', {
//       templateUrl : './pages/patients/addPatient.html',
//       //controller  : 'stockController'
//     })
//
// //servder eka up karalada iyene ? ne up karala yanna localhost
// });

// angular.module('PharmacyApp').config(['$routeProvider',
//     function ($routeProvider) {
//         $routeProvider
//             .when('/', {
//                 templateUrl: 'pages/dashboard/dashboard.html',
//                 controller: 'MainController'
//             })
//             .when('/addPatient', {
//                 templateUrl: './pages/patients/addPatient.html',
//                 controller: 'PatientController'
//             });
//
//             // $locationProvider.html5Mode(true);
//             // routerApp.config(function($stateProvider, $urlRouterProvider) {
//     //
//     // $urlRouterProvider.otherwise('/');
//     //
//     // $stateProvider
//     //         .state('dashboard', {
//     //           url: '/'
//     //           templateUrl: 'pages/dashboard/dashboard.html',
//     //           controller: 'MainController'
//     //         })
//     //         .state('app.patients', {
//     //           url: "/patients",
//     //           abstract:true,
//     //           template: '<ui-view></ui-view>',
//     //           ncyBreadcrumb: {
//     //             label: "Patients"
//     //           }
//     //         })
//     //         .state('app.patients.add-patients', {
//     //           url: '/patients/addPatient',
//     //           templateUrl: 'pages/patients/addPatient.html',
//     //           ncyBreadcrumb: {
//     //             label: "Add Patients"
//     //           }
//     //         })
//             // .when('/suppliers',{
//             //     templateUrl: 'index.html',
//             //     controller: 'SupplierController'
//             // })
//         //     .otherwise({
//         //         redirectTo: '/'
//         // });
//
//         // $locationProvider.html5Mode(true);
//     }]);

// configure our routes
PharmacyApp.config(function($routeProvider) {
  $routeProvider

    // Routes for the home page/stock page
    .when('/', {
    	templateUrl : 'pages/stock/stock.html',
    	controller  : 'StockController'
    })
    .when('/stock', {
      templateUrl : 'pages/stock/stock.html',
      controller  : 'StockController'
    })


    // Route for the patients page
    .when('/patients', {
      templateUrl : 'pages/patients/patient.html',
      controller  : 'PatientController'
    })

    .when('/drugbill', {
      templateUrl : 'pages/bill/drugbill.html',
      //controller  : 'stockController'
    })
    // .when('/addSupplier', {
    //   templateUrl : 'pages/supplier/addSupplier.html',
    //   //controller  : 'stockController'
    // })
    .when('/viewSupplier', {
      templateUrl : 'pages/supplier/viewSupplier.html',
      //controller  : 'stockController'
    })
    // .when('/addPharmacist', {
    //   templateUrl : 'pages/pharmacist/addPharmacist.html',
    //   //controller  : 'stockController'
    // })
    .when('/viewPharmacist', {
      templateUrl : 'pages/pharmacist/viewPharmacist.html',
      //controller  : 'stockController'
    })



});
