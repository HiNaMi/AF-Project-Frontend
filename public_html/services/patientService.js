'use strict';

angular.module('PharmacyApp').factory('PatientService', ['$http',
    function ($http) {
        return {
            get: () => $http.get('http://localhost:3000/patients').then(response => response.data),
            add: patient => $http.post('http://localhost:3000/patients/create', patient).then(response => response.data),
            // getById: id => $http.get('/drivers/' + id).then(response => response.data),
            // addComment: (id, comment) => $http.post('/drivers/' + id + '/comments', comment).then(response => response.data),
        };
        
    }]);
