$(function () {
    $('.js-basic-example').DataTable({
        responsive: true
    });

    //Exportable table
    $('.js-exportable').DataTable({
        dom: 'Bfrtip',
        responsive: true,
        buttons: [
            'excel', 'pdf', 'print'
        ],
    //    columns[
      //    {data:"#"}
      //  ]
    });
});


angular.module('drugs', [])
  .controller('DrugController', function($scope,$http) {
      $scope.abc="123-34";
      $scope.model={drugList:[],selected:{}};
      //$scope.id=0;

      $scope.addNewDrug=function (){
          $scope.model.drugList.push({id:$scope.id++,name:$scope.drug.name,category:'',unit:'',type:'',reorder:$scope.drug.reOrder,danger:$scope.drug.danger,remark:$scope.drug.remark});
          //console.log(JSON.stringify($scope.model.drugList));
      };

      $scope.updateDrug=function (drug){
          $scope.model.selected = angular.copy(drug);
          $scope.editable=true;
      };

      $scope.deleteDrug=function (){

      };

      $scope.saveChanges=function (index){
          $scope.model.drugList[index] = angular.copy($scope.model.selected);
        $scope.resetChanges();
      };
      $scope.resetChanges=function (){
          $scope.model.selected = {};
          $scope.editable=false;
      };

       // gets the template to ng-include for a table row / item
    $scope.getTemplate = function (drug) {
        if (drug.id === $scope.model.selected.id) return 'edit';
        else return 'display';
    };
});
